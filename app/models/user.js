var exports = module.exports = {};
var users = [
				{name: 'Lucas', password: '1234'},
				{name: 'Claudio', password: '1234'},
				{name: 'Carlos', password: '1234'},
				{name: 'Diogo', password: '1234'},
				{name: 'Leo', password: '1234'},
			]

exports.find = function(user, callback) {
	return callback(200, users);
}

exports.findOne = function(user, callback) {
	var result = users.find(u => u.name === user.name);
	if(!result) return callback(null);
	return callback(null, result);
}
