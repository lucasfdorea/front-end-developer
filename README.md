# Desafio Front-end Developer #

O desafio consiste em fazer um site simples que integre com o servidor Node.js provido neste repositório para autenticação, utilizando **Angular 1, ou 2**. Uma vez autenticado, a api provê uma pequena lista com nomes e senhas de usuários. O site deve ser **responsivo**.

### O que deve ser feito? ###

* Uma página de login utilizando o serviço /api/authenticate informando usuário e senha conforme exemplo:
```
#!html
POST /api/authenticate HTTP/1.1
Host: localhost:3000
Cache-Control: no-cache
Content-Type: application/x-www-form-urlencoded
Body:
name=Diogo&password=1234

```

* Uma página principal com identificação do usuário autenticado e um menu (lateral, ou no topo, à sua escolha)

### Como configurar o ambiente? ###

* Instale o Node.js
* Faça um fork deste repositório
* Faça clone do seu fork
* Execute o comando npm install
* Execute o comando node app.js para iniciar o servidor
* /api/authenticate gera um token para autenticação do usuário, adicionando o token no header, você poderá usar a listagem.
```
#!json
{
    "success": true,
    "message": "Enjoy your token!",
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRGlvZ28iLCJwYXNzd29yZCI6IjEyMzQiLCJpYXQiOjE0OTg3Njg3NzMsImV4cCI6MTQ5ODc2OTc3M30.8o7WH1X1Q9eOAfam3Q9CZmUxsxW9gZgesGcVuyBWgPw"
}
```
* /api/users lista todos os usuários da base, como no exemplo abaixo:

```
#!html
GET /api/users HTTP/1.1
Host: localhost:3000
x-access-token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRGlvZ28iLCJwYXNzd29yZCI6IjEyMzQiLCJpYXQiOjE0OTg3Njg3NzMsImV4cCI6MTQ5ODc2OTc3M30.8o7WH1X1Q9eOAfam3Q9CZmUxsxW9gZgesGcVuyBWgPw
Cache-Control: no-cache
```